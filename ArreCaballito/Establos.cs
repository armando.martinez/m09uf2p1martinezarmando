﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace ArreCaballito
{
    public class Caballo
    {
        public string Nombre { get; set; }
        public int Posicion { get; set; }
        public int Velocidad { get; set; }
        private Random rand { get; set; }
        public Caballo(string nombre)
        {
            this.Nombre = nombre;
            this.rand = new Random();
            this.Posicion = 1;
            GearChange();
        }

        public void GearChange()
        {
            this.Velocidad = rand.Next(1, 8);
        }

        public void Visualize()
        {
            if (this.Posicion + this.Velocidad < 100) this.Posicion += this.Velocidad;
            else this.Posicion = 100;

            string track = "";
            for (int i = 0; i < this.Posicion; i++) { track += "-"; }

            Console.WriteLine($"{this.Nombre}:\n\t|{track}");

            this.Velocidad--;
            if (this.Velocidad <= 0) GearChange();
        }

        public void Run()
        {
            do
            {
                this.Visualize();
                Thread.Sleep(1000);
            } while (this.Posicion < 100);
        }
    }
    public class Carrera
    {
        public List<Thread> Threads { get; set; }
        public Carrera(List<Thread> threads) 
        {
            this.Threads = threads;
        }
        
        public void Start()
        {
            do
            {
                Console.WriteLine("La gran carrera intercontinental!");
                Thread.Sleep(1000);
                Console.Clear();

            } while (IsThreadsAlive());
        }

        public bool IsThreadsAlive()
        {
            foreach(Thread t in this.Threads)
            {
                if (t.IsAlive == true) return true;
            }
            return false;
        }
    }
}