﻿using System;
using System.Threading;
using System.Collections.Generic;


namespace ArreCaballito
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WindowWidth = 200;
            Caballo[] establo = new Caballo[] 
            {
                new Caballo("Rocinante"),
                new Caballo("Valkyrie"),
                new Caballo("Vicente"),
                new Caballo("Spirit"),
                new Caballo("Sardinilla"),
                new Caballo("Torrentera"),
                new Caballo("Epona") 
            };

            List<Thread> threads = new List<Thread>();
            foreach (Caballo c in establo)
            {
                Thread.Sleep(5);
                Thread t = new Thread(c.Run);
                threads.Add(t);
            }

            Carrera sbr = new Carrera(threads);
            Thread thread = new Thread(sbr.Start);
            if (Console.ReadKey().Key == ConsoleKey.C)
            {
                thread.Start();
                Thread.Sleep(5);
                foreach (Thread t in threads)
                {
                    t.Start();
                    Thread.Sleep(5);
                }
            }


        }
    }
}
